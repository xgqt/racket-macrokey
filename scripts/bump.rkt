#!/usr/bin/env racket


;; This file is part of racket-macrokey.

;; racket-macrokey is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-macrokey is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-macrokey.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang racket/base

(require
 (only-in racket/file file->lines)
 (only-in racket/port display-lines)
 "../src/macrokey-lib/macrokey/version.rkt"
 )


(define srcdir
  (build-path (current-directory) "src"))

(define info-files
  (map
   (lambda (p) (build-path p "info.rkt"))
   (filter directory-exists? (directory-list srcdir #:build? #t))
   ))


(module+ main

  (for ([f info-files])
    {define updated
      (map (lambda (l) (if (regexp-match "define version" l)
                      (regexp-replace #rx"(?<=version ).*(?=\\))"
                                      l (format "~s" VERSION))
                      l
                      ))
           (file->lines f)
           )}
    (with-output-to-file
      f (lambda () (display-lines updated)) #:exists 'replace)
    )

  )
