# This file is part of racket-macrokey.

# racket-macrokey is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-macrokey is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-macrokey.	 If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later


MAKE			:= make
RACKET			:= racket
RACO			:= raco
SCRIBBLE		:= $(RACO) scribble

# For recursive calls
PACKAGE			:=


DO-DOCS			:= --no-docs
INSTALL-FLAGS	:= --auto --skip-installed $(DO-DOCS)

DEPS-FLAGS		:= --check-pkg-deps --unused-pkg-deps
SETUP-FLAGS		:= --tidy --avoid-main $(DEPS-FLAGS)

TEST-FLAGS		:= --heartbeat --no-run-if-absent --submodule test --table


clean-package:
	find ./$(PACKAGE) -type d -name 'compiled' -exec rm -dr {} +

clean-macrokey:
	$(MAKE) clean-package PACKAGE="macrokey"
clean-macrokey-doc:
	$(MAKE) clean-package PACKAGE="macrokey-doc"
clean-macrokey-lib:
	$(MAKE) clean-package PACKAGE="macrokey-lib"
clean-macrokey-test:
	$(MAKE) clean-package PACKAGE="macrokey-test"

clean-base: clean-macrokey-lib
clean-extras: clean-macrokey-test clean-macrokey-doc
clean-meta: clean-base clean-extras clean-macrokey
clean: clean-meta


compile-package:
	$(RACKET) -e "(require compiler/compiler setup/getinfo) (compile-directory-zos (path->complete-path \"$(PACKAGE)\") (get-info/full \"$(PACKAGE)/info.rkt\") #:skip-doc-sources? #t #:verbose #f)"

compile-macrokey:
	$(MAKE) compile-package PACKAGE="macrokey"
compile-macrokey-doc:
	$(MAKE) compile-package PACKAGE="macrokey-doc"
compile-macrokey-lib:
	$(MAKE) compile-package PACKAGE="macrokey-lib"
compile-macrokey-test:
	$(MAKE) compile-package PACKAGE="macrokey-test"

compile-base: compile-macrokey-lib
compile-extras: compile-macrokey-test compile-macrokey-doc
compile-meta: compile-base compile-extras compile-macrokey
compile: compile-meta


install-package:
	cd ./$(PACKAGE) && $(RACO) pkg install $(INSTALL-FLAGS)

install-macrokey:
	$(MAKE) install-package PACKAGE="macrokey"
install-macrokey-doc:
	$(MAKE) install-package PACKAGE="macrokey-doc"
install-macrokey-lib:
	$(MAKE) install-package PACKAGE="macrokey-lib"
install-macrokey-test:
	$(MAKE) install-package PACKAGE="macrokey-test"

install-base: install-macrokey-lib
install-extras: install-macrokey-test install-macrokey-doc
install-meta: install-base install-extras install-macrokey
install: install-meta


setup-package:
	$(RACO) setup $(SETUP-FLAGS) --pkgs $(PACKAGE)

setup-macrokey: install-macrokey
	$(MAKE) setup-package PACKAGE="macrokey"
setup-macrokey-doc: install-macrokey-doc
	$(MAKE) setup-package PACKAGE="macrokey-doc"
setup-macrokey-lib: install-macrokey-lib
	$(MAKE) setup-package PACKAGE="macrokey-lib"
setup-macrokey-test: install-macrokey-test
	$(MAKE) setup-package PACKAGE="macrokey-test"

setup-base: setup-macrokey-lib
setup-extras: setup-macrokey-test setup-macrokey-doc
setup-meta: setup-base setup-extras setup-macrokey
setup: setup-meta


test-package:
	$(RACO) test $(TEST-FLAGS) --package $(PACKAGE)

test-macrokey:
	$(MAKE) test-package PACKAGE="macrokey"
test-macrokey-doc:
	$(MAKE) test-package PACKAGE="macrokey-doc"
test-macrokey-lib:
	$(MAKE) test-package PACKAGE="macrokey-lib"
test-macrokey-test:
	$(MAKE) test-package PACKAGE="macrokey-test"

test-base: test-macrokey-lib
test-extras: test-macrokey-test test-macrokey-doc
test-meta: test-base test-extras test-macrokey
test: test-meta


remove-package:
	$(RACO) pkg remove $(DO-DOCS) $(PACKAGE)

remove-macrokey:
	$(MAKE) remove-package PACKAGE="macrokey"
remove-macrokey-doc:
	$(MAKE) remove-package PACKAGE="macrokey-doc"
remove-macrokey-lib:
	$(MAKE) remove-package PACKAGE="macrokey-lib"
remove-macrokey-test:
	$(MAKE) remove-package PACKAGE="macrokey-test"

remove-base: remove-macrokey-lib
remove-extras: remove-macrokey-doc remove-macrokey-test
remove-meta: remove-macrokey remove-base
remove: remove-meta


purge-macrokey: remove-macrokey clean-macrokey
purge-macrokey-doc: remove-macrokey-doc clean-macrokey-doc
purge-macrokey-lib: remove-macrokey-lib clean-macrokey-lib
purge-macrokey-test: remove-macrokey-test clean-macrokey-test

purge-base: purge-macrokey-lib
purge-extras: purge-macrokey-doc purge-macrokey-test
purge-meta: purge-macrokey purge-base
purge: purge-meta


doc-clean:
	rm -dfr ./macrokey-doc/macrokey/doc

doc:
	$(MAKE) setup-macrokey-doc SETUP-FLAGS="--doc-index --no-launcher --no-pkg-deps"

doc-regen: doc-clean doc
