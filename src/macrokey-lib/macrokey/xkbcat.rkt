#!/usr/bin/env racket


;; This file is part of racket-macrokey.

;; racket-macrokey is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-macrokey is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-macrokey.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang racket/base

(require
 racket/class
 "private/keyaction.rkt"
 )

(provide (all-defined-out))


(define xkbcat-path
  (make-parameter
   (cond
     [(getenv "XKBCAT_DIR") => (lambda (s) (build-path s "xkbcat"))]
     [(find-executable-path "xkbcat")]
     [else  (displayln "; WARNING: Could not find xkbcat")  ""]
     )
   ))

(define xkbcat%
  (class object%

    (super-new)

    (define-values (sp stdout etdin stderr)
      (subprocess #f #f #f (xkbcat-path) "-up"))

    (define/public (get-line)
      (read-line stdout 'linefeed)
      )

    (define/public (get-keyaction)
      (string->keyaction (send this get-line))
      )

    (define/public (get-keyaction-values)
      (let ([current-keyaction  (send this get-keyaction)])
        (values
         (keyaction-key      current-keyaction)
         (keyaction-position current-keyaction)
         (keyaction-action   current-keyaction)
         )))

    (define/public (get-key)
      (keyaction-key      (send this get-keyaction))
      )

    (define/public (get-position)
      (keyaction-position (send this get-keyaction))
      )

    (define/public (get-action)
      (keyaction-action   (send this get-keyaction))
      )

    ))


#|
;; Read keys in a lopp
(begin (define x (new xkbcat%))
(let loop ()  (displayln (send x get-line))  (loop) ))
|#
