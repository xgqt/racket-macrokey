MAKE		:= make
RACKET		:= racket
RACO		:= raco
SCRIBBLE	:= $(RACO) scribble
SH		:= sh

# For recursive calls
WHAT		:=


all: compile


macrokey-make:
	cd ./src && $(MAKE) DEPS-FLAGS=" --no-pkg-deps " $(WHAT)

macrokey-clean:
	$(MAKE) macrokey-make WHAT=clean

clean: macrokey-clean bin-clean public-clean

macrokey-compile:
	$(MAKE) macrokey-make WHAT=compile

compile: macrokey-compile

macrokey-install:
	$(MAKE) macrokey-make WHAT=install

install: macrokey-install

macrokey-setup:
	$(MAKE) macrokey-make WHAT=setup

setup: macrokey-setup

macrokey-test:
	$(MAKE) macrokey-make WHAT=test

test-full: macrokey-test

macrokey-remove:
	$(MAKE) macrokey-make WHAT=remove

remove: macrokey-remove

macrokey-purge:
	$(MAKE) macrokey-make WHAT=purge

purge: macrokey-purge


test-unit:
	$(SH) ./scripts/test.sh unit

test-integration:
	$(SH) ./scripts/test.sh integration

test: test-unit test-integration


public:
	$(SH) ./scripts/public.sh

public-clean:
	rm -dfr ./public

public-regen: public-clean public
